import { Component, OnInit } from '@angular/core';
import { AuthUtilService } from '../auth/auth-util.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(public authUtilService: AuthUtilService) { }

  ngOnInit() {
  }
}
