export const API = {
    default: 'http://192.168.103.4:8080/quadro-api/api/'
}

export function getDefaultUrl(resource: string): string {
    return API.default + resource;
}